dnl
dnl Process this file with autoconf to produce a configure script
dnl
AC_INIT([Debian posh],POSH_VERSION,[schizo@debian.org],[posh])
AC_PREREQ(2.59)
AM_INIT_AUTOMAKE
AC_CONFIG_HEADERS([config.h])
AC_GNU_SOURCE
AC_PROG_MAKE_SET

dnl
dnl
AC_PROG_CC
AC_PROG_GCC_TRADITIONAL
AC_PROG_CPP
dnl
dnl  Set up command line options (--enable/--disable)
dnl
def_path_unix="/bin:/usr/bin:/usr/ucb"
def_path_os2="c:/usr/bin;c:/os2;/os2"
enable_shell=posh
AC_ARG_ENABLE(path,
[  --enable-path=PaTh      (NOTE: this value isn't used if confstr() and _CS_PATH
                          are available, or if <paths.h> defines _PATH_DEFPATH)
                          Use PaTh if PATH isn't specified in the environment
                          when the shell starts.  A value without . in it is
                          safest.
                          The default value is \"/bin:/usr/bin:/usr/ucb\".],,
                enable_path=default)
case $enable_path:$ksh_cv_os_type in
 default:OS2_EMX) enable_path="$def_path_os2" ;;
 default:*) enable_path="$def_path_unix" ;;
esac
case $enable_path in
  \"*\") ;;
  *)
    enable_path="\"$enable_path\""
    ;;
esac
AC_DEFINE_UNQUOTED(DEFAULT_PATH, $enable_path, [default hardcoded PATH])
dnl
dnl
dnl
dnl  Specify what kind of shell we are to build.  Options are ksh and sh.
dnl  This must be before most other options, as it controls their default
dnl  behaviour.
dnl
SHELL_PROG=posh
AC_SUBST(SHELL_PROG)
dnl
dnl
dnl
dnl
dnl
dnl
dnl
AC_ARG_ENABLE(posixly_correct,
[  --enable-posixly-correct Enable if you want POSIX behavior by default
                          (otherwise, posix behavior is only turned on if the
                          environment variable POSIXLY_CORRECT is present or by
                          using \"set -o posix\"; it can be turned off with
                          \"set +o posix\").  See the POSIX Mode section in the
                          man page for details on what this option affects.
                          NOTE:  posix mode is not compatable with some bourne
                          sh/at&t ksh scripts.])
case $enable_posixly_correct:$enable_shell in
  yes:*) enable_posixly_correct=yes; AC_DEFINE(POSIXLY_CORRECT, 1, [POSIX behavior]) ;;
  no:*|:*) enable_posixly_correct=no;;
  *) AC_MSG_ERROR(bad --enable-posixly_correct argument)
esac
dnl
dnl
AC_ARG_ENABLE(default-env,
[  --enable-default-env=FILE Include FILE if ENV parameter is not set when
                          the shell starts.  This can be useful when used with
                          rsh(1), which creates a non-login shell (ie, profile
                          isn't read, so there is no opertunity to set ENV).
                          Setting ENV to null disables the inclusion of
                          DEFAULT_ENV.  NOTE: This is a non-standard feature
                          (ie, at&t ksh has no default environment).],,
  enable_default_env=no)
if test X"$enable_default_env" != Xno; then
  # The [a-zA-Z]:/ is for os2 types...
  case $enable_default_env in
    /*|[[a-zA-Z]]:/*)
      enable_default_env="\"$enable_default_env\""
      ;;
    \"/*\"|\"[[a-zA-Z]]:/*\")
      ;;
    *)
      AC_MSG_ERROR(--enable-default-env argument must be an absolute path (was $enable_default_env))
      ;;
  esac
  AC_DEFINE_UNQUOTED(DEFAULT_ENV, $enable_default_env, [enable default environment])
fi
dnl
dnl
dnl Don't want silly documented - its silly
AC_ARG_ENABLE(silly,[  --enable-silly          [A silly option]])
case $enable_silly:$enable_shell in
  yes:*) enable_silly=yes; AC_DEFINE(SILLY, 1, [oookay]) ;;
  no:*|:*) enable_silly=no;;
  *) AC_MSG_ERROR(bad --enable-silly argument)
esac
dnl
dnl
dnl don't want swtch documented - its ancient and probably doesn't work
AC_ARG_ENABLE(swtch,
[  --enable-swtch          For use with shell layers (shl(1)).  This has not
                          been tested for some time.])
case $enable_swtch:$enable_shell in
  yes:*) enable_swtch=yes; AC_DEFINE(SWTCH, 1, [swtch]) ;;
  no:*|:*) enable_swtch=no;;
  *) AC_MSG_ERROR(bad --enable-swtch argument)
esac
dnl
dnl
dnl  Start of auto-configuration stuff...
dnl
dnl
dnl A hack to turn on warning messages for gcc - Warn-flags is not in
dnl the distribution since not everyone wants to see this stuff.
dnl (Warn-flags contains: -Wall)
if test X"$GCC" = Xyes && test -f $srcdir/Warn-flags; then
  CFLAGS="${CFLAGS+$CFLAGS }`cat $srcdir/Warn-flags`"
fi

dnl
dnl If LDSTATIC set in environment, pass it on to the Makefile and use it when
dnl doing compile checks to ensure we are checking the right thing.
AC_SUBST(LDSTATIC)LDSTATIC=${LDSTATIC-}
test X"$LDSTATIC" != X && LDFLAGS="${LDFLAGS+$LDFLAGS }$LDSTATIC"
dnl
dnl Executable suffix - normally empty; .exe on os2.
AC_SUBST(ac_exe_suffix)dnl

dnl
dnl Program name munging stuff (prefix, suffix, transform)
dnl AC_ARG_PROGRAM
dnl
dnl
dnl Headers
dnl
AC_HEADER_STDC
AC_HEADER_DIRENT
dnl KSH_TERM_CHECK
AC_SYS_POSIX_TERMIOS
AC_CHECK_HEADERS(stddef.h stdlib.h memory.h fcntl.h limits.h paths.h \
	sys/param.h sys/resource.h values.h ulimit.h sys/time.h unistd.h \
	termio.h termios.h)
AC_HEADER_TIME
AC_HEADER_SYS_WAIT
dnl
dnl
dnl Typedefs
dnl
dnl (don't use AC_TYPE_XXX() 'cause it doesn't check word boundaries)
AC_CHECK_TYPE(off_t)
AC_CHECK_TYPE(mode_t)
AC_CHECK_TYPE(pid_t)
AC_CHECK_TYPE(uid_t)
dnl if test $ac_cv_type_uid_t = no; then
dnl   AC_DEFINE(gid_t, int)
dnl fi
define([AC_PROVIDE_AC_TYPE_UID_T],)
AC_TYPE_SIGNAL
case $ac_cv_type_signal in
int)  ksh_cv_signal_retval=0 ;;
void) ksh_cv_signal_retval=  ;;
*)
    AC_MSG_ERROR(Internal erorr: unknown signal return type: $ac_cv_type_signal)
esac
AC_DEFINE_UNQUOTED(RETSIGVAL, $ksh_cv_signal_retval, [retsigval])
AC_CHECK_SIZEOF(int)
AC_CHECK_SIZEOF(long)
dnl sh.h sets INT32 to int or long as appropriate.  Some burnt systems, such
dnl as NeXT's, clock_t is in sys/time.h (in non-posix mode).
dnl KSH_CHECK_H_TYPE(clock_t, [[for clock_t in any of <sys/types.h>, <sys/times.h> and <sys/time.h>]],
dnl   [
dnl #ifdef HAVE_SYS_TIME_H
dnl #include <sys/time.h>
dnl #endif /* HAVE_SYS_TIME_H */
dnl #include <sys/times.h>
dnl 	], INT32)
dnl KSH_CHECK_H_TYPE(sigset_t, for sigset_t in <sys/types.h> and <signal.h>,
dnl   [#include <signal.h>], unsigned)
dnl KSH_RLIM_CHECK
AC_CHECK_TYPE(sigset_t)
AC_CHECK_TYPE(rlim_t)
dnl
dnl
dnl Library functions
dnl
AC_CHECK_FUNCS(confstr dup2 flock getcwd getwd killpg nice \
	setrlimit sysconf tcsetpgrp \
	ulimit waitpid wait3 lstat setpgrp tcsetattr \
	get_current_dir_name canonicalize_file_name)
AC_FUNC_MMAP
AC_FUNC_SETPGRP
AC_FUNC_STAT
AC_FUNC_LSTAT
dnl KSH_SYS_ERRLIST
dnl KSH_SYS_SIGLIST
dnl KSH_TIME_DECLARED
dnl KSH_TIMES_CHECK
dnl
dnl
dnl Structures
dnl
AC_HEADER_STAT
dnl
dnl
dnl Compiler characteristics
dnl
AC_C_CONST
dnl KSH_C_VOID
dnl KSH_C_VOLATILE
dnl KSH_C_PROTOTYPES
dnl KSH_C_FUNC_ATTR
dnl
dnl
dnl System services
dnl
AC_SYS_INTERPRETER
if test $ac_cv_sys_interpreter = no;
  then AC_DEFINE(SHARPBANG, 1, [sharpbang])
fi
AC_PROG_INSTALL
dnl
dnl
dnl  Misc ksh tests
dnl
dnl KSH_DUP2_CLEXEC_CHECK
dnl KSH_SIGNAL_CHECK
dnl KSH_OPENDIR_CHECK
dnl KSH_DEV_FD
dnl
dnl
dnl Take replace value of LDSTATIC in LDFLAGS with reference to make variable
if test X"$LDSTATIC" != X; then
  LDFLAGS=`echo -- "$LDFLAGS" | sed -e 's/^-- //' -e 's?$LDSTATIC?\$(LDSTATIC)?'`
fi
dnl
AC_CONFIG_FILES(Makefile src/Makefile tests/Makefile)
AC_OUTPUT

name: expand-1
description:
	Check for overzealous tilde expansion
category: debian,posix
stdin:
	echo f~
	echo f=~
	echo f==~
expected-stdout:
	f~
	f=~
	f==~
---
name: expand-2
description:
	Check trimming
stdin:
	x=fooXbarXblah
	echo 1 ${x%X*}
	echo 2 ${x%%X*}
	echo 3 ${x#*X}
	echo 4 ${x##*X}
expected-stdout: 
	1 fooXbar
	2 foo
	3 barXblah
	4 blah
---
name: expand-3
category: pdksh
description:
	Check ${foo:%bar} is not allowed (ksh88 allows it...)
stdin:
	x=fooXbarXblah
	echo 1 ${x%X*}
	echo 2 ${x:%X*}
	echo 3 ${x:%%X*}
	echo 4 ${x:#*X}
	echo 5 ${x:##*X}
expected-exit: e = 1
expected-stdout: 
	1 fooXbar
expected-stderr-pattern: /.*bad substitution.*/
---

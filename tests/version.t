name: version-1
description:
	Check version of shell.
category: posh
stdin:
	echo $POSH_VERSION
expected-stdout:
	0.8.4
---

name: arithmetic-1
description:
	Addition
category: debian,posix
stdin:
	echo $(( 100 + 200 ))
expected-stdout:
	300
---
name: arithmetic-2
description:
	Subtraction
category: debian,posix
stdin:
	echo $(( 100 - 200 ))
expected-stdout:
	-100
---
name: arithmetic-3
description:
	Multiplication
category: debian,posix
stdin:
	echo $(( 100 * 200 ))
expected-stdout:
	20000
---
name: arithmetic-4
description:
	Division
category: debian,posix
stdin:
	echo $(( 200 / 100 ))
expected-stdout:
	2
---
name: arithmetic-bases-1
description:
	Check input bases
category: debian,posix
stdin:
	echo $(( 50 ))
	echo $(( 050 ))
	echo $(( 0x50 ))
expected-stdout:
	50
	40
	80
---
name: arithmetic-bases-2
description:
	Check mixed bases
category: debian,posix
stdin:
	echo $(( 50 - 050 ))
	echo $(( 050 * 0xa ))
	echo $(( 0x50 / 010 ))
expected-stdout:
	10
	400
	10
---

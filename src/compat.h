#ifndef HAVE_GET_CURRENT_DIR_NAME
char *get_current_dir_name (void);
#endif /* HAVE_GET_CURRENT_DIR_NAME */

#ifndef CANONICALIZE_FILE_NAME
char *canonicalize_file_name (const char *name);
#endif /* CANONICALIZE_FILE_NAME */
